import os
import time
import signal
import sys
os.makedirs("log", exist_ok=True)
f = open("log/demofile.txt","w")


def handler(signum, frame):
     exit(0)
 
signal.signal(signal.SIGINT, handler)
 
for i in range(200):
    time.sleep(0.001)
    f.write("train step3: " + str(i) + " \n")
    f.flush()

f.close()